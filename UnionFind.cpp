/*******************************************************************************
* File UnionFind.cpp
*
* Goal: Implement the UnionFind data structure and functionality
*
* Created January 16th  - Anthony Kougkas
* (C) 2016 
******************************************************************************/
/******************************************************************************
*include files
******************************************************************************/
#include "UnionFind.h"

/******************************************************************************
*Constructor
******************************************************************************/
UnionFind::UnionFind() {
  numSets = 0;
  map = std::unordered_map<int, Node>();
}

/******************************************************************************
*Destructor
******************************************************************************/
UnionFind::~UnionFind() { }

/******************************************************************************
*Function contains()
 * Overview: Returns whether the set contains the node.
******************************************************************************/
bool UnionFind::contains(int node) {
  return map.find(node) != map.end();
}

/******************************************************************************
*Function makeSet()
 * Overview: Adds the node as a singleton set.
******************************************************************************/
void UnionFind::makeSet(int node) {
/* Do nothing if already contains this node */
  if (!contains(node)) {
    // Make struct to store info about this node
    Node n;
    n.parent = node;
    n.rank = 0;
    // Store in map
    map[node] = n;
    // Increment number of sets
    numSets++;
  }
}

/******************************************************************************
*Function size()
 * Overview: Returns size of disjoint-sets.
******************************************************************************/
int UnionFind::size() {
  return numSets;
}

/******************************************************************************
*Function find()
 * Overview: Returns the root node of a provided node. Compresses path along
 * the way, flattening the map by setting each node visited along the path to
 * the root to link directly to the root.
******************************************************************************/
int UnionFind::find(int node) {
  if (map[node].parent != node) map[node].parent = find(map[node].parent);
  return map[node].parent;
}

/******************************************************************************
*Function link()
 * Overview: Joins together the two sets to which two nodes belong. Uses
 * weighted union approach (smaller tree added to bigger tree).
******************************************************************************/
void UnionFind::link(int node1, int node2) {
  int root1 = find(node1);
  int root2 = find(node2);
  if (root1 != root2) {
    // Link the smaller tree to the bigger tree (weighted)
    if (map[root1].rank < map[root2].rank)
      map[root1].parent = root2;
    else if (map[root2].rank < map[root1].rank)
      map[root2].parent = root1;
      // Else, trees of equal size; increment rank
    else {
      map[root2].parent = root1;
      map[root1].rank++;
    }
    // Decrement number of sets
    numSets--;
  }
}

/******************************************************************************
*Function removeSet()
 * Overview: Removes all nodes on paths from s and from t from the UnionFind
 * data structure.
******************************************************************************/
void UnionFind::removeSet(std::unordered_set<int> &s,
                          std::unordered_set<int> &t) {
  for (auto it = s.begin(); it != s.end(); ++it) {
    map.erase(*it);
  }
  for (auto it = t.begin(); it != t.end(); ++it) {
    map.erase(*it);
  }
  numSets--; // Decrement number of sets
}
