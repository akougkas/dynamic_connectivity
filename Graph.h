/*******************************************************************************
* File Graph.h
*
* Goal: Provide a clean interface to user to check connectivity in a dynamically
* changing graph.
*
* Created January 16th  - Anthony Kougkas
* (C) 2016
******************************************************************************/
#ifndef DYNAMIC_CONNECTIVITY_GRAPH_H
#define DYNAMIC_CONNECTIVITY_GRAPH_H
/******************************************************************************
*include files
******************************************************************************/
#include <vector>
#include <map>
#include <unordered_set>
#include "UnionFind.h"

/******************************************************************************
*Class Graph
******************************************************************************/
class Graph {

public:
/******************************************************************************
*Constructor
******************************************************************************/
  Graph() {
    graphStructure = std::unordered_multimap<int, int>();//Initialize an empty
    // graph
    UF = UnionFind();//Initialize an empty UnionFind data structure
  }

/******************************************************************************
*Destructor
******************************************************************************/
  ~Graph() { }

/******************************************************************************
*Interface operations
******************************************************************************/

/******************************************************************************
*Function addEdge()
 * Goal: Add an edge into the graph, from source s to target t
 * Input arguments: int source vertex, int target vertex
 * Return: void
 * Usage: g.addEdge(s,t)
******************************************************************************/
  void addEdge(int s, int t);

/******************************************************************************
*Function removeEdge()
 * Goal: Remove edge from source s to target t off the graph
 * Input arguments: int source vertex, int target vertex
 * Return: void
 * Usage: g.removeEdge(s,t)
******************************************************************************/
  void removeEdge(int s, int t);

/******************************************************************************
*Function countConnectedComponents()
 * Goal: Return to the user the number of connected components in the graph
 * Input arguments: nothing
 * Return: an integer representing the number of connected components
 * Usage: g.countConnectedComponents()
******************************************************************************/
  int countConnectedComponents();

/******************************************************************************
*Private members
******************************************************************************/
private:
/******************************************************************************
*Helper functions
******************************************************************************/

/******************************************************************************
*Function searchGraph()
 * Goal: Execute a Bread First Search on the graph starting from start vertex
 * Input arguments: the starting vertex (integer)
 * Return: a vector holding the path explored by BFS
 * Usage: std::vector<int> path = searchGraph(s);
******************************************************************************/
  std::unordered_set<int> searchGraph(int start);

/******************************************************************************
*Function addInGraph()
 * Goal: Add edge s-t into the graph
 * Input arguments: int source vertex, int target vertex
 * Return: true if successful, false if edge already in graph
 * Usage: bool success = addInGraph(s,t);
******************************************************************************/
  bool addInGraph(int s, int t);

/******************************************************************************
*Function removeFromGraph()
 * Goal: Remove edge s-t from the graph
 * Input arguments: int source vertex, int target vertex
 * Return: true if successful, false if edge not in graph
 * Usage: bool success = removeFromGraph(s,t);
******************************************************************************/
  bool removeFromGraph(int s, int t);

/******************************************************************************
*Function updateSet()
 * Goal: Updates the UnionFind data structure by removing a set and adding two
 * new sets. Called after a deletion of an edge that disconnected the graph.
 * Input arguments: Two sets of integers representing paths from s and t
 * accordingly
 * Return: void
 * Usage: updateSet(s_path, t_path);
******************************************************************************/
  void updateSet(std::unordered_set<int> &s_path,
                 std::unordered_set<int> &t_path);

/******************************************************************************
*Function isConnected()
 * Goal: Check if there is a path between two vertices s and t
 * Input arguments: int source vertex, int target vertex, set of int to
 * store the path if exists
 * Return: true if there is a path connecting s and t, false otherwise
 * Usage: bool success = isConnected(s,t, s_path);
******************************************************************************/
  bool isConnected(int s, int t, std::unordered_set<int> &path);

/******************************************************************************
*Function printGraph()
 * Goal: Print the graph
 * Input arguments: empty
 * Return: void
 * Usage: printGraph()
******************************************************************************/
  void printGraph();

/******************************************************************************
*Variables
******************************************************************************/
  //Object from class UnionFind, to be used to hold the connected components
  UnionFind UF;
  //A map to hold the graph structure
  std::unordered_multimap<int, int> graphStructure;

};

#endif //DYNAMIC_CONNECTIVITY_GRAPH_H
