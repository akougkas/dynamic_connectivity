Anthony Kougkas, PhD 
Illinois Institute of Technology, 2016
###############################################################################
Design and Implementation Exercise 
###############################################################################
Scenario
Dynamically processing complex data sets as quickly as possible. 
Consider the hypothetical requirement to count the number
of disconnected sub-graphs of an undirected graph.
###############################################################################
Task
Write a component that keeps track of a dynamically changing graph and reports
the number of disconnected sub-graphs. The graph can have an arbitrary number
of vertices and each vertex can have an arbitrary number of edges. Focus on
efficient algorithms, a clean interface, and production-worthy code.
To test your component write a main that reads from standard input the set of
edges that describe the graph and writes to standard output the number of
disconnected sub-graphs. The set of edges are specified by numbered vertices,
one edge defined on each line.
###############################################################################
Sample Input
1 2
3 4
4 5
5 6
6 3
7 8
7 9
10 7
Sample Output
3
###############################################################################
Environment
Write portable, standard C++ code – avoid using non-standard compiler extensions
 and libraries.
###############################################################################
Deliverables
1. Maintainable source code (no binaries are needed)
2. A document discussing:

a. Design choices
b. Assumptions
c. Time and space complexity trade-offs
d. Compiler name and version

###############################################################################
###############################################################################

    $$$$$$$$$$$$$$$$$$$$$$$$$$--DESIGN CHOICES--$$$$$$$$$$$$$$$$$$$$$$$$$$$

This problem appears in the literature as dynamic connectivity.
Problem Statement: The set V of vertices of the graph is fixed, but the set E
of edges can change. The three cases, in order of difficulty, are:
-Edges are only added to the graph (this can be called incremental)
-Edges are only deleted from the graph (this can be called decremental)
-Edges can be either added or deleted (this can be called fully dynamic)

In this problem, I offer a solution for the fully dynamic connectivity.

There have been a lot of research papers on the topic that offer state-of-the-
art algorithmic solutions. Some of the papers discussing the issue are:

-Thorup, Mikkel. "Near-optimal fully-dynamic graph connectivity."
Proceedings of the thirty-second annual ACM symposium on Theory of computing.
ACM, 2000.

-Zavlanos, Michael M., and George J. Pappas. "Controlling connectivity of
dynamic graphs." IEEE Conference on Decision and Control.
IEEE, 2005

-Henzinger, Monika R., and Valerie King. "Randomized fully dynamic graph
algorithms with polylogarithmic time per operation."
Journal of the ACM (JACM) 46.4 (1999): 502-516.

-Jacob Holm, Kristian de Lichtenberg, and Mikkel Thorup. 2001. Poly-logarithmic
deterministic fully-dynamic algorithms for connectivity, minimum spanning tree,
2-edge, and biconnectivity.
J. ACM 48, 4 (July 2001), 723-760

Summarizing the above papers, there are several results for dynamic connectivity
in specific types of graphs.
-Trees.
For trees, we can support O(log n) time for queries and updates using Euler-Tour
trees or Link-Cut trees. If the trees are only decremental, then we can support
queries in constant time.

-Planar graphs.
Eppstein et al. have proven that we can attain O(log n) queries and updates.

-General dynamic graphs.
In 2000 Thorup showed how to obtain O(logn*(loglogn)^3) updates and
O(logn/logloglogn) queries.
Holm, de Lichtenberg, and Thorup obtained O(log^2 n) updates and O(logn/loglogn)
queries.
For the incremental data structure, the best result we have is Θ(α(m, n)) using
union-find.
It remains an open problem whether O(log n) queries and updates are attainable
for general graphs. All of the aforementioned runtimes are amortized.

In my solution, I aimed for efficiency and I implemented a mixture of the
above. For addition of edges (i.e. incremental connectivity), I implemented a
solution using the Union Find data structure which has the best running time.
For deletion of edges, I chose the less efficient (but faster to implement) way
of running a graph exploration algorithm like Breadth First Search or Depth
First Search to check if the number of connected components is changing after
the deletion. Worst case running time is in the order of #of vertices in the
smaller resulting component if the edge removal disconnected the graph. 
This project is organized in two classes:
- Graph class
Implements the graph structure and offers an interface to the user.
- UnionFind class
Implements the Union Find (i.e. Disjoint Sets) data structure.

###############################################################################

    $$$$$$$$$$$$$$$$$$$$$$$$$$$--ASSUMPTIONS--$$$$$$$$$$$$$$$$$$$$$$$$$$$

* No multiple edges are allowed.
graph.addEdge(1,2); // adds edge 1-2 (and 2-1 since undirected)
graph.addEdge(1,2); // does nothing, edge already in the graph
...
graph.addEdge(1,3); // adds edge 1-3 (and 3-1 since undirected)


* Self loops are ignored since they don't change connectivity
graph.addEdge(1,2); // adds edge 1-2 (and 2-1 since undirected)
graph.addEdge(1,1); // does nothing, self loop
...
graph.addEdge(1,3); // adds edge 1-3 (and 3-1 since undirected)


* Node ID (i.e., keys in map) is an integer value
For the sake of simplicity, i assume in this implementation that nodes are
integers but in a general graph structure, we would make it a generic data
structure using
templates.

###############################################################################

    $$$$$$$$$$$$$$$$$$$$$$$$$$$--COMPLEXITY--$$$$$$$$$$$$$$$$$$$$$$$$$$$

Time:
addEdge() - Sub_logarithmic O(a(n)) (n is #vertices) amortized

removeEdge() - Linear O(m+n) (m edges and n vertices of the component we remove
 the edge from)

countConnectedComponent() - Constant O(1)

Space:
UnionFind class - O(m+n) (m edges and n vertices of the graph)
Graph class - O(m+n) (m edges and n vertices of the graph)


###############################################################################

    $$$$$$$$$$$$$$$$$$$$$$$$$--COMPILER INFO--$$$$$$$$$$$$$$$$$$$$$$$$$$$

gcc version 4.8.4 (Ubuntu 4.8.4-2ubuntu1~14.04)

CMake Version 3.3

Code was implemented using the IDE software Clion 1.2.4 by JetBrains

