/*******************************************************************************
* File UnionFind.h
*
* Goal: UnionFind data structure(i.e. Disjoint Sets)
* Optimizations: It includes weighted union and path compression features
* Running Time: On M union-find operations on N objects: O(N + MlogN).
* Implementation: Disjoint-set forests where each set is represented by a tree
* data structure, in which each node holds a reference to its parent node.
*
* Created January 16th  - Anthony Kougkas
* (C) 2016
******************************************************************************/
#ifndef DYNAMIC_CONNECTIVITY_UNIONFIND_H
#define DYNAMIC_CONNECTIVITY_UNIONFIND_H
/******************************************************************************
*include files
******************************************************************************/
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <vector>

/******************************************************************************
*Class UnionFind
******************************************************************************/
class UnionFind {

public:
/******************************************************************************
*Constructor
******************************************************************************/
  UnionFind();

/******************************************************************************
*Destructor
******************************************************************************/
  ~UnionFind();

/******************************************************************************
*UnionFind operations
******************************************************************************/

/******************************************************************************
*Function contains()
 * Goal: Checks if node is in the data structure
 * Input arguments: A pointer to integer that represents the node ID
 * Return: True if node exists, false otherwise
 * Usage: bool exists = UnionFind object.contains(node);
******************************************************************************/
  bool contains(int node);

/******************************************************************************
*Function makeSet()
 * Goal: Adds a singleton to the disjoint-sets
 * Input arguments: A pointer to integer that represents the node ID to be added
 * Return: void
 * Usage: UnionFind object.makeSet(node);
******************************************************************************/
  void makeSet(int node);

/******************************************************************************
*Function size()
 * Goal: Returns size of disjoint-sets (i.e. number of connected components)
 * Input arguments: empty
 * Return: An integer for number of connected components
 * Usage: UnionFind object.size();
******************************************************************************/
  int size();

/******************************************************************************
*Function find()
 * Goal: Finds the root node
 * Input arguments: A pointer to integer that represents the node ID to find
 * Return: An integer the node ID of the root of node
 * Usage: int root = UnionFind object.find(s);
******************************************************************************/
  int find(int node);

/******************************************************************************
*Function link()
 * Goal: Joins together the two sets to which two nodes belong
 * Input arguments: Two integers representing the nodes to be linked
 * Return: void
 * Usage: UnionFind object.link(s,t);
******************************************************************************/
  void link(int node1, int node2);

/******************************************************************************
*Function removeSet()
 * Goal: Removes the disjoint set of root(s)
 * Input arguments: Two sets of integers representing nodes in path s-t
 * Return: void
 * Usage: UnionFind object.removeSet(s_path,t_path);
******************************************************************************/
  void removeSet(std::unordered_set<int> &s, std::unordered_set<int> &t);

/******************************************************************************
*Private members
******************************************************************************/
private:
  struct Node {
    //Node structure with parent and rank
    int parent;
    int rank;
  };
  int numSets;
  // Number of sets in disjoint-sets
  std::unordered_map<int, Node> map;// Map of KeyType to Node
};


#endif //DYNAMIC_CONNECTIVITY_UNIONFIND_H
