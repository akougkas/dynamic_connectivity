/*******************************************************************************
* File Test_UnionFind.cpp
*
* Created January 16th  - Anthony Kougkas
* (C) 2016 
******************************************************************************/
/******************************************************************************
*include files
******************************************************************************/
#include <iostream>
#include "test_unionfind.h"

int testUnionFind(int testcase) {

  UnionFind testUF = UnionFind();

  switch (testcase) {
    case 1: {
      testUF.makeSet(1);
      testUF.makeSet(2);
      assert(testUF.contains(1));
      assert(testUF.contains(2));
      break;
    }
    case 2: {
      testUF.makeSet(1);
      testUF.makeSet(2);
      std::unordered_set<int> node_list1 = std::unordered_set<int>();
      std::unordered_set<int> node_list2 = std::unordered_set<int>();
      node_list1.insert(1);
      node_list2.insert(2);

      testUF.removeSet(node_list1, node_list2);
      assert(!testUF.contains(1) && !testUF.contains(2));
      break;
    }
    case 3: {
      testUF.makeSet(1);
      testUF.makeSet(2);
      int root1 = testUF.find(1);
      int root2 = testUF.find(2);
      assert(root1 == 1 && root2 == 2);
      break;
    }
    case 4: {
      testUF.makeSet(1);
      testUF.makeSet(2);

      testUF.link(1, 2);
      int root1 = testUF.find(1);
      int root2 = testUF.find(2);

      assert(root1 == root2);
      break;
    }
    case 5: {
      testUF.makeSet(1);
      testUF.makeSet(2);
      int sets_before = testUF.size();

      testUF.link(1, 2);
      int sets_after = testUF.size();

      assert(sets_before != sets_after);
      break;
    }
    default:
      break;
  }
}