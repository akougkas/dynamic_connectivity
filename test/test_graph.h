/*******************************************************************************
* File Test_Graph.cpp
 * Goal: Test the helper functions from Graph class
 * Test Cases:
 * #1: Cycle graph
 * #2: Tree
*
* Created January 16th  - Anthony Kougkas
* (C) 2016
******************************************************************************/
#ifndef DYNAMIC_CONNECTIVITY_TEST_GRAPH_H
#define DYNAMIC_CONNECTIVITY_TEST_GRAPH_H
/******************************************************************************
*include files
******************************************************************************/
#include "../Graph.h"
#include <assert.h>

/******************************************************************************
*Functions
******************************************************************************/
void testCycle(int numVertices);
void testTree(int numNodes);
void testUserInput(int numTries);

#endif //DYNAMIC_CONNECTIVITY_TEST_GRAPH_H
