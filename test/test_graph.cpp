/*******************************************************************************
* File Test_Graph.cpp
*
* Created January 16th  - Anthony Kougkas
* (C) 2016 
******************************************************************************/
/******************************************************************************
*include files
******************************************************************************/
#include <iostream>
#include "test_graph.h"

/******************************************************************************
*Functions
******************************************************************************/

/******************************************************************************
*Function testCycle()
 * Overview: Creates a graph first where each node is connected with the next
 * one and previous one. Then it adds one edge from the last to the first node
 * making the graph a cycle. Finally, it starts removing edges until all nodes
 * are disconnected and we get a graph of numVertices+1 nodes and 0 edges.
******************************************************************************/
void testCycle(int numVertices) {
  Graph g = Graph();

  // test additions
  for (int i = numVertices; i > 1; i--) {
    g.addEdge(i, i - 1);
    assert(g.countConnectedComponents() == 1);
  }
  g.addEdge(1, numVertices);
  assert(g.countConnectedComponents() == 1);

  // test deletions
  g.removeEdge(numVertices, 1);
  assert(g.countConnectedComponents() == 1);

  for (int i = 1; i < numVertices; i++) {
    g.removeEdge(i, i + 1);
    assert(g.countConnectedComponents() == i+1);
  }
}
/******************************************************************************
*Function testTree()
 * Overview: Creates a binary tree and then disconnects the left subtree from
 * the root first and finally the right subtree.
******************************************************************************/
void testTree(int numNodes) {
  Graph g = Graph();
  int j= 0;
  for(int i = 1; i<numNodes; i=i+2){
    g.addEdge(j,i);
    g.addEdge(j,i+1);
    j++;
    assert(g.countConnectedComponents()==1);
  }
  g.removeEdge(0,1);//Cut off left subtree from root
  assert(g.countConnectedComponents() == 2);
  g.removeEdge(0,2);//Cut off right subtree from root
  assert(g.countConnectedComponents() == 3);
}

/******************************************************************************
*Function testTree()
 * Overview: Creates a binary tree and then disconnects the left subtree from
 * the root first and finally the right subtree.
******************************************************************************/
void testUserInput(int numTries) {
  Graph g = Graph();

  int s,t;
  int efforts = numTries;
  std::cout << "Enter "<<numTries<< " edges to be added: " << std::endl;
  while(efforts){
    std::cin >> s >> t;
    g.addEdge(s,t);
    std::cout << "#of CCs: " << g.countConnectedComponents() << std::endl;
    efforts--;
  }
  efforts = numTries;
  std::cout << "Enter "<<numTries<< " edges to be deleted: " << std::endl;
  while(efforts){
    std::cin >> s >> t;
    g.removeEdge(s,t);
    std::cout << "#of CCs: " << g.countConnectedComponents() << std::endl;
    efforts--;
  }
}
