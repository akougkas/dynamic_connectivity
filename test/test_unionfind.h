/*******************************************************************************
* File TestUnionFind.h
 * Goal: Test the UnionFind data structure and functionality
 * Test Cases:
 * #1: Insert singletons (i.e., add nodes) to the set
 * #2: Remove nodes from set
 * #3: Find representative of set (i.e., root)
 * #4: Link nodes together
 * #5: Get the number of sets
*
* Created January 16th  - Anthony Kougkas
* (C) 2016 
******************************************************************************/
#ifndef DYNAMIC_CONNECTIVITY_TEST_UNIONFIND_H
#define DYNAMIC_CONNECTIVITY_TEST_UNIONFIND_H
/******************************************************************************
*include files
******************************************************************************/
#include "../UnionFind.h"
#include <assert.h>

/******************************************************************************
*Functions
******************************************************************************/
int testUnionFind(int testcase);


#endif //DYNAMIC_CONNECTIVITY_TEST_UNIONFIND_H
