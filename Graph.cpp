/*******************************************************************************
* File Graph.cpp
*
* Goal: Implement functions from interface
*
* Created January 16th  - Anthony Kougkas
* (C) 2016 
******************************************************************************/
/******************************************************************************
*include files
******************************************************************************/
#include "Graph.h"
#include <queue>

/******************************************************************************
****************************Interface operations*******************************
******************************************************************************/

/******************************************************************************
*Function addEdge()
 * Overview: First adds vertices s and t into the graph structure.If successful,
 * it then adds these vertices into the UnionFind data structure as singleton
 * nodes. Finally, it links the two vertices together, creating the edge.
******************************************************************************/
void Graph::addEdge(int s, int t) {

  if (addInGraph(s, t)) {//Add s and t in graph
    //Add s and t to UnionFind data structure as singleton nodes
    UF.makeSet(s);
    UF.makeSet(t);
    UF.link(s, t);//Link s and t into an edge s-t
  }
}

/******************************************************************************
*Function removeEdge()
 * Overview: First removes edge s-t from the graph structure. Upon successful
 * removal, if graph gets disconnected because of this edge removal, it updates
 * the UnionFind data structure by first removing the set containing all nodes
 * before removing the edge and adding the two new sets. To discover the
 * connectivity of the two new components, it runs BFS starting from each end
 * of the edge that was removed and uses the path discovered to rebuild the set.
******************************************************************************/
void Graph::removeEdge(int s, int t) {

  if (removeFromGraph(s, t)) {//Remove edge s-t from the graph
    //Sets to store the path discovered from BFS
    std::unordered_set<int> s_path;
    std::unordered_set<int> t_path;
    if (!isConnected(s, t, s_path)) {//Check for connectivity
      t_path = searchGraph(t);//If disconnected, run BFS from t too
      updateSet(s_path, t_path);//Update the UnionFind structure
    }
  }
}

/******************************************************************************
*Function countConnectedComponents()
 * Overview: Returns the number of connected components by checking the size
 * of the UnionFind data structure.
******************************************************************************/
int Graph::countConnectedComponents() {
  return UF.size();
}

/******************************************************************************
******************************Helper functions*********************************
******************************************************************************/

/******************************************************************************
*Function searchGraph()
 * Overview: Implements a Breadth First Search of the graph. Utilizes a queue
 * to check visited nodes. Complexity is linear O(#of nodes)
******************************************************************************/
std::unordered_set<int> Graph::searchGraph(int start) {
  //To use for visited nodes
  std::unordered_set<int> visited = std::unordered_set<int>();

  std::queue<int> queue;
  queue.push(start);//Start with the node that BFS initiates the search
  visited.insert(start); //Add root to the set

  while (!queue.empty()) {
    start = queue.front();
    queue.pop();
    //Loop through the graph structure to get the children and visit them
    std::pair<std::unordered_multimap<int, int>::iterator,
        std::unordered_multimap<int, int>::iterator> map_iter;
    map_iter = graphStructure.equal_range(start);
    for (std::unordered_multimap<int, int>::iterator it = map_iter.first;
         it != map_iter.second; ++it) {
      if (visited.find(it->second) == visited.end()) {
        queue.push(it->second);
        visited.insert(it->second);
      }
    }
  }
  return visited;
}

/******************************************************************************
*Function addInGraph()
 * Overview: Adds edge s-t to the graph structure. It checks if the edge is
 * already in the graph and then inserts it.
******************************************************************************/
bool Graph::addInGraph(int s, int t) {
  if (s == t) {//Ignore self loops
    return false;
  }
  //Check if map is empty
  if (graphStructure.size() == 0) {
    graphStructure.insert(std::pair<int, int>(s, t));
    graphStructure.insert(std::pair<int, int>(t, s));
    return true;
  }
  //Check if s exists in the map
  bool found_edge = false;
  std::pair<std::unordered_multimap<int, int>::iterator,
      std::unordered_multimap<int, int>::iterator> contains_it;
  contains_it = graphStructure.equal_range(s);
  for (std::unordered_multimap<int, int>::iterator it = contains_it.first;
       it != contains_it.second; ++it) {
    if (it->second == t) found_edge = true;
  }
  if (!found_edge) {
    graphStructure.insert(std::pair<int, int>(s, t));
    graphStructure.insert(std::pair<int, int>(t, s));
    return true;
  }
  return false;
}


/******************************************************************************
*Function removeFromGraph()
 * Overview: Removes edge s-t to the graph structure. It checks if the edge
 * exists in the graph and then removes it.
******************************************************************************/
bool Graph::removeFromGraph(int s, int t) {

  bool found_edge = false;
  //Start by search s in the map
  std::pair<std::unordered_multimap<int, int>::iterator,
      std::unordered_multimap<int, int>::iterator> contains_it;
  contains_it = graphStructure.equal_range(s);

  for (std::unordered_multimap<int, int>::iterator it = contains_it.first;
       it != contains_it.second; it++) {
    if (it->second == t) {
      graphStructure.erase(it);
      found_edge = true;
      break;
    }
  }
  if (found_edge) {
    contains_it = graphStructure.equal_range(t);
    for (std::unordered_multimap<int, int>::iterator it = contains_it.first;
         it != contains_it.second; ++it) {
      if (it->second == s) {
        graphStructure.erase(it);
        return true;
      }
    }
  }
  return false;
}

/******************************************************************************
*Function updateSet()
 * Overview: Removes all nodes in the set prior to the deletion of an edge.
 * It then pushes back the vertices into the UnionFind data structure and
 * finally it links nodes according the edges still in the graph. Utilizes
 * removeSet() function of UnionFind class and helper function recreateSet to
 * link vertices together.
******************************************************************************/
void Graph::updateSet(std::unordered_set<int> &s_path,
                      std::unordered_set<int> &t_path) {
  //Removes all nodes present in the paths from UnionFind data structure
  UF.removeSet(s_path, t_path);
  //Create singleton nodes in UnionFind data structure for both paths
  for (std::unordered_set<int>::iterator it = s_path.begin();
       it != s_path.end(); ++it) {
    UF.makeSet(*it);
  }
  for (std::unordered_set<int>::iterator it = t_path.begin();
       it != t_path.end(); ++it) {
    UF.makeSet(*it);
  }
  //Rebuild the UnionFind structure by linking edges still in graph after
  // deletion of edge s-t
  int root = *s_path.begin();
  for (std::unordered_set<int>::iterator it = s_path.begin();
       it != s_path.end(); ++it) {
    UF.link(root, *it);
  }
  root = *t_path.begin();
  for (std::unordered_set<int>::iterator it = t_path.begin();
       it != t_path.end(); ++it) {
    UF.link(root, *it);
  }
}

/******************************************************************************
*Function isConnected()
 * Overview: Checks connectivity between two vertices (i.e. if there exists a
 * path between s-t) by calling searchGraph() (i.e. BFS).
******************************************************************************/
bool Graph::isConnected(int s, int t, std::unordered_set<int> &path) {
  path = searchGraph(s);//Run BFS from s
  return path.find(t) != path.end();//If t was visited return true
}

/******************************************************************************
*Function printGraph()
 * Overview: Prints the edges of the graph
******************************************************************************/
void Graph::printGraph() {
  for (auto iter = graphStructure.begin();
       iter != graphStructure.end(); ++iter) {
    std::cout << iter->first << "->" << iter->second << std::endl;
  }
}
